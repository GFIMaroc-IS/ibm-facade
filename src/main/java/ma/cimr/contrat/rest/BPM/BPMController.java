package ma.cimr.contrat.rest.BPM;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/BPM")
public class BPMController {

	@GetMapping("/submitTask")
	public void submitTask() {
		
	}
	
	@GetMapping("/tasks")
	public void getTasks() {
		
	}
	
	
}
