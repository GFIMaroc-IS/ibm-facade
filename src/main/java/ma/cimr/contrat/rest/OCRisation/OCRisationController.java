package ma.cimr.contrat.rest.OCRisation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ma.cimr.contrat.service.OCRisation.OCRisationService;


@RestController
@RequestMapping("/OCR")
public class OCRisationController {

	@Autowired
	private OCRisationService ocrService;
	
	@PostMapping( value= "/readFile", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public String readFile(@RequestParam("file") MultipartFile file) {
		String result = ocrService.getFileText(file);
		return result;
	}

	public OCRisationService getOcrService() {
		return ocrService;
	}

	public void setOcrService(OCRisationService ocrService) {
		this.ocrService = ocrService;
	}

}
