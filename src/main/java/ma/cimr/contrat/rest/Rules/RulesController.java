package ma.cimr.contrat.rest.Rules;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.cimr.contrat.service.Rules.RulesService;


@RestController
@RequestMapping("/rules")
public class RulesController {

	@Autowired
	private RulesService rulesService;
	
	private final static Logger logger =LoggerFactory.getLogger(RulesController.class.getName()); 
	
	@PostMapping("/validateAdhesion")
	public ResponseEntity<String> validateAdhesion(@RequestBody Map<String, String> parameters) {
		logger.info("Rules Controller : Start validating Adhesion request");
		JSONObject ddeAdhesionReq = new JSONObject();
		try {
			JSONObject adherent = new JSONObject();
			adherent.put("Effectif", parameters.get("effectif"));
			adherent.put("FormeJuridique", parameters.get("formeJuridique"));
			ddeAdhesionReq.put("adherent", adherent);
			ddeAdhesionReq.put("__DecisionID__", parameters.get("__DecisionID__"));
		} catch (JSONException e) {
			logger.error("Paramètres erronés de la requete");
		}
		JSONObject result  = rulesService.validateDemandeAdhesion(ddeAdhesionReq);
		return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
	}
	
	
}
