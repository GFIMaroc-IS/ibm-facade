package ma.cimr.contrat.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ma.cimr.contrat.common.Constants;

public class DateUtil {

	public static Date getFinDuMois(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		return cal.getTime();
	}
	
	public static Date stringToDate(String sDate, String sFormat)  {
		Date date = null;
		if(sDate != null && !sDate.equals("")){
			SimpleDateFormat sdf = new SimpleDateFormat((sFormat != null && !sFormat.equals(""))? sFormat : Constants.DATE_FORMAT);
			try {
				date = sdf.parse(sDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return date;
	}
	
	public static String dateToString(Date date, String sFormat)  {
		String str = null;
		if(date != null ){
			SimpleDateFormat sdf = new SimpleDateFormat((sFormat != null && !sFormat.equals(""))? sFormat : Constants.DATE_FORMAT);
			str = sdf.format(date);
		}
		return str;
	}
	
	public static void main(String... args) {
		String str = "30/01/2018";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = dateFormat.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		System.out.print(Integer.toString(cal.get(Calendar.MONTH)));
	}
}
