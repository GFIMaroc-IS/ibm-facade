package ma.cimr.contrat.util;

import java.io.IOException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class StringUtil {

	public static boolean isEmpty(String str) {

		if (str == null) {
			return true;
		}
		if (str.trim().equals("")) {
			return true;
		}
		return false;
	}

	public static String nullIfEmpty(String str) {
		if (isEmpty(str))
			return null;
		return str;
	}

	public static String encodeStringBCrypt(String str) {

		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String bCryptedPassword = bCryptPasswordEncoder.encode(str);

		return bCryptedPassword;

	}
}
