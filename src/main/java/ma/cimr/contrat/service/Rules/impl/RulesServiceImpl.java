	package ma.cimr.contrat.service.Rules.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.service.Rules.RulesService;
import ma.cimr.contrat.service.util.ApiUtils;

@Service
public class RulesServiceImpl implements RulesService {

	private static final Logger logger = LoggerFactory.getLogger(RulesServiceImpl.class.getName());
	
	@Override
	public JSONObject validateDemandeAdhesion(JSONObject ddeAdhesionReq) {
		logger.info("Start validation de la demande d'Adhésion");
		
		JSONObject result = new JSONObject();
		
		String urlODM = ApplicationConfig.getODMURL();

		try {
			JSONObject ddeAdhesionRep = ApiUtils.post(ddeAdhesionReq, urlODM);
			JSONObject produit = (JSONObject)ddeAdhesionRep.get("adherent");
			if (produit != null) {
				String statut = (String) produit.get("status");
				String message = (String) produit.get("Produit");
				
				if ("Rejected".equalsIgnoreCase(statut) || ((message != null) && message.trim().length()>0)) {	
					result.put("status", "KO");
					result.put("message", message);					
				} else {
					result.put("status", "OK");
					result.put("message", "");					
				}
			} else {
				result.put("status", "KO");
				result.put("message", "");				
			}
		} catch (JSONException e) {
			logger.error("Erreur d'invocation du moteur de règles", e);
		} catch (Exception e) {
			logger.error("Erreur d'invocation du moteur de règles", e);
		}
		
		return result;
	}
}
