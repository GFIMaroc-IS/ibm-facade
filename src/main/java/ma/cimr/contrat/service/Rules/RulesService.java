package ma.cimr.contrat.service.Rules;

import org.springframework.boot.configurationprocessor.json.JSONObject;

public interface RulesService {

	public JSONObject validateDemandeAdhesion(JSONObject ddeAdhesionReq);
	

}
