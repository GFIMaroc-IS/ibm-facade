package ma.cimr.contrat.service.OCRisation.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.service.OCRisation.OCRisationService;

@Service
public class OCRisationServiceImpl implements OCRisationService {

	private static final Logger logger = Logger.getLogger(OCRisationServiceImpl.class.getName());
	
	public static final String BASE_URL = "http://135.125.31.131:82/Service/";
	public static final String APPLICATION ="APT";
	public static final String USER ="admin";
	public static final String PASSWORD ="admin";
	public static final String STATION ="1";
	public static final String WORKFLOW ="APT";
	public static final String PAGEFILENAME ="transaction";
	public static final String RULESETS ="PageID,CreateDocs,Recognize";
	public static final String IMAGENAME ="ICE";
	public static final String IMAGEEXT ="pdf";
	//public static final String IMAGEPATH ="C:\\Datacap\\APT\\Images\\Input\\APT001.tif";
	public static final String IMAGEPATH ="C:\\Users\\Administrator\\Downloads\\ToBeUploaded\\ICE.pdf";

	String pageFile = "<B id=\"APT\"><V n=\"TYPE\">APT</V><P id=\"APT001\"><V n=\"TYPE\">Other</V>" +
			"<V n=\"IMAGEFILE\">ICE.pdf</V></P></B>";
	

	public void processTransaction() {
				
		logger.info("Start processTransaction");
		
		String transactionId = ""; //id returned by the Start method to be included with subsequent calls
				
		CloseableHttpClient httpclient = HttpClients.createDefault();
			
		try {
			//log on and get wTmId header value to include in following requests
			//if you can restrict access to the service using another method like kerberos or ip filtering
			//then disable transaction security so you do not need Datacap Server or database connections
			//by setting transactionSecurity = False in web.config
			//The cookie wTmId is returned and must be included with all following requests.
			//Using the same CloseableHttpClient instance will persist the value.
			//If using another client, or instance of the client for following requests you may need to extract the ID and add it with following requests
			String logon = "<LogonProperties>" +
					"<application>APT</application>" +
					"<password>admin</password>" +
					"<station>1</station>" +
					"<user>admin</user>" +
					"</LogonProperties>";
			String urlLogon = BASE_URL +  "Session/Logon";
			HttpPost httpPostLogon = new HttpPost(urlLogon);
			httpPostLogon.setEntity(new StringEntity(logon));
			httpPostLogon.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseLogon = httpclient.execute(httpPostLogon);
			StatusLine statusLineLogon = responseLogon.getStatusLine();
			responseLogon.close();
			System.out.println("logon status: " + statusLineLogon.getStatusCode());	
			
			//create new transaction
			HttpGet httpget = new HttpGet( BASE_URL + "Transaction/Start");
			//this example uses XML but you can use json instead by setting the accept header
			//httpget.addHeader("accept", "application/json");
			CloseableHttpResponse responseStart = httpclient.execute(httpget);
			HttpEntity entityStart = responseStart.getEntity();
			if (entityStart != null) {
				InputStream instream = entityStart.getContent();
				String entityValueStart = EntityUtils.toString(entityStart);
				if(entityValueStart == null)
					return;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(new InputSource(new ByteArrayInputStream(entityValueStart.getBytes("utf-8"))));
				Element rootElement = document.getDocumentElement();
				transactionId = rootElement.getTextContent();
				instream.close();
			}					
			responseStart.close();
			System.out.println("transactionId: " + transactionId);
		    
			//upload file
			File fileUploadImage = new File(IMAGEPATH);
			String setfileUrlUploadImage = BASE_URL +  "Transaction/SetFile/" + transactionId + "/" + IMAGENAME + "/" + IMAGEEXT;
			HttpPost httpPostUploadImage = new HttpPost(setfileUrlUploadImage);
			MultipartEntityBuilder builderUploadImage = MultipartEntityBuilder.create();
			builderUploadImage.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builderUploadImage.addBinaryBody("upload file", fileUploadImage, ContentType.DEFAULT_BINARY, fileUploadImage.getName());
			
			HttpEntity reqEntityUploadImage = builderUploadImage.build();
			httpPostUploadImage.setEntity(reqEntityUploadImage);
			CloseableHttpResponse responseUploadImage = httpclient.execute(httpPostUploadImage);
			//entityStart = responseUploadImage.getEntity();
			StatusLine statusLineUploadImage = responseUploadImage.getStatusLine();
			responseUploadImage.close();
			System.out.println("upload file status: " + statusLineUploadImage.getStatusCode());
			
			//upload page file
			//the page file is the index file containing a list of files that are uploaded
			String setfileUrlUploadPageFile = BASE_URL +  "Transaction/SetFile/" + transactionId + "/" + PAGEFILENAME + "/xml";
			HttpPost httpPostUploadPageFile = new HttpPost(setfileUrlUploadPageFile);
			MultipartEntityBuilder builderUploadPageFile = MultipartEntityBuilder.create();
			builderUploadPageFile.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builderUploadPageFile.addTextBody("scan.xml", pageFile);
			HttpEntity reqEntityUploadPageFile = builderUploadPageFile.build();
			httpPostUploadPageFile.setEntity(reqEntityUploadPageFile);
			CloseableHttpResponse responseUploadPageFile = httpclient.execute(httpPostUploadPageFile);
			StatusLine statusLinePageFile = responseUploadPageFile.getStatusLine();
			responseUploadPageFile.close();
			System.out.println("upload page file status: " + statusLinePageFile.getStatusCode());
			
			//execute rules
			String executeRules = "<Properties>" +
					"<TransactionId>" + transactionId + "</TransactionId>" +
					"<Application>" + APPLICATION + "</Application>" +
					"<Workflow>" + WORKFLOW + "</Workflow>" +
					"<PageFile>transaction.xml</PageFile>" +
					"<Rulesets>" + RULESETS + "</Rulesets>" +
					"</Properties>";
			String urlExecuteRules = BASE_URL +  "Transaction/Execute";
			HttpPost httpPostExecuteRules = new HttpPost(urlExecuteRules);
			httpPostExecuteRules.setEntity(new StringEntity(executeRules));
			httpPostExecuteRules.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseExecuteRules = httpclient.execute(httpPostExecuteRules);
			StatusLine statusLineExecuteRules = responseExecuteRules.getStatusLine();
			responseUploadPageFile.close();
			System.out.println("execute rules status: " + statusLineExecuteRules.getStatusCode());
			
			//get a file produced by rules
			HttpGet httpGetFile = new HttpGet(BASE_URL + "Transaction/GetFile/" + transactionId + "/" + IMAGENAME + "/txt");
			CloseableHttpResponse responseGetFile = httpclient.execute(httpGetFile);
			HttpEntity entityGetFile = responseGetFile.getEntity();
			String getFileText = "";
			
			if (entityGetFile != null) {
				// get entity contents and convert it to string
				InputStream	instream = entityGetFile.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(instream,"utf8"),8192);
				String str;
				while ((str = br.readLine())!=null)
				{
					getFileText +=str;
				}
				//getFileText = EntityUtils.toString(entityGetFile);
				instream.close();
			}	
			StatusLine statusLineGetFile = responseExecuteRules.getStatusLine();
			responseGetFile.close();
			System.out.println("get file status: " + statusLineGetFile.getStatusCode());
			System.out.println("get file text:");
			System.out.println("*****************************");
			System.out.println(getFileText);
			System.out.println("*****************************");
			
			//end transaction
			HttpDelete httpEnd = new HttpDelete(BASE_URL + "Transaction/End/"+transactionId);
			CloseableHttpResponse responseEnd = httpclient.execute(httpEnd);
			StatusLine statusLineEnd = responseEnd.getStatusLine();
			responseEnd.close();
			System.out.println("end status: " + statusLineEnd.getStatusCode());
			
			//logoff
			String urlLogoff = BASE_URL +  "Session/Logoff";
			HttpPost httpPostLogoff = new HttpPost(urlLogoff);
			httpPostLogoff.setEntity(new StringEntity(""));
			httpPostLogoff.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseLogoff = httpclient.execute(httpPostLogoff);
			StatusLine statusLineLogoff = responseLogon.getStatusLine();
			responseLogoff.close();
			System.out.println("logoff status: " + statusLineLogoff.getStatusCode());	
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String args[])
    {
		OCRisationServiceImpl client = new OCRisationServiceImpl();
		client.processTransaction();
    }
	
	@Override
	public String getFileText(MultipartFile mpFile) {
		logger.info("Start validation de la demande d'Adhésion");
		
		String result = null;
		
		String urlOCR = ApplicationConfig.getOCRURL();

		try {
			logger.info("Start processTransaction");
				
			String transactionId = ""; //id returned by the Start method to be included with subsequent calls
					
			CloseableHttpClient httpclient = HttpClients.createDefault();
			String logon = "<LogonProperties>" +
					"<application>APT</application>" +
					"<password>admin</password>" +
					"<station>1</station>" +
					"<user>admin</user>" +
					"</LogonProperties>";
			String urlLogon = urlOCR +  "Session/Logon";
			HttpPost httpPostLogon = new HttpPost(urlLogon);
			httpPostLogon.setEntity(new StringEntity(logon));
			httpPostLogon.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseLogon = httpclient.execute(httpPostLogon);
			StatusLine statusLineLogon = responseLogon.getStatusLine();
			responseLogon.close();
			logger.info("logon status: " + statusLineLogon.getStatusCode());	

			HttpGet httpget = new HttpGet( urlOCR + "Transaction/Start");

			CloseableHttpResponse responseStart = httpclient.execute(httpget);
			HttpEntity entityStart = responseStart.getEntity();
			if (entityStart != null) {
				InputStream instream = entityStart.getContent();
				String entityValueStart = EntityUtils.toString(entityStart);
				if(entityValueStart == null)
					return result;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(new InputSource(new ByteArrayInputStream(entityValueStart.getBytes("utf-8"))));
				Element rootElement = document.getDocumentElement();
				transactionId = rootElement.getTextContent();
				instream.close();
			}					
			responseStart.close();
			logger.info("transactionId: " + transactionId);

			File fileUploadImage = new File("c:/temp/ICE.pdf");
			mpFile.transferTo(fileUploadImage);
			String setfileUrlUploadImage = urlOCR +  "Transaction/SetFile/" + transactionId + "/" + IMAGENAME + "/" + IMAGEEXT;
			HttpPost httpPostUploadImage = new HttpPost(setfileUrlUploadImage);
			MultipartEntityBuilder builderUploadImage = MultipartEntityBuilder.create();
			builderUploadImage.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			builderUploadImage.addBinaryBody("upload file", fileUploadImage, ContentType.DEFAULT_BINARY, fileUploadImage.getName());
			
			HttpEntity reqEntityUploadImage = builderUploadImage.build();
			httpPostUploadImage.setEntity(reqEntityUploadImage);
			CloseableHttpResponse responseUploadImage = httpclient.execute(httpPostUploadImage);
			//entityStart = responseUploadImage.getEntity();
			StatusLine statusLineUploadImage = responseUploadImage.getStatusLine();
			responseUploadImage.close();
			logger.info("upload file status: " + statusLineUploadImage.getStatusCode());
			
			//upload page file
			//the page file is the index file containing a list of files that are uploaded
			String setfileUrlUploadPageFile = urlOCR +  "Transaction/SetFile/" + transactionId + "/" + PAGEFILENAME + "/xml";
			HttpPost httpPostUploadPageFile = new HttpPost(setfileUrlUploadPageFile);
			MultipartEntityBuilder builderUploadPageFile = MultipartEntityBuilder.create();
			builderUploadPageFile.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builderUploadPageFile.addTextBody("scan.xml", pageFile);
			HttpEntity reqEntityUploadPageFile = builderUploadPageFile.build();
			httpPostUploadPageFile.setEntity(reqEntityUploadPageFile);
			CloseableHttpResponse responseUploadPageFile = httpclient.execute(httpPostUploadPageFile);
			StatusLine statusLinePageFile = responseUploadPageFile.getStatusLine();
			responseUploadPageFile.close();
			logger.info("upload page file status: " + statusLinePageFile.getStatusCode());
			
			//execute rules
			String executeRules = "<Properties>" +
					"<TransactionId>" + transactionId + "</TransactionId>" +
					"<Application>" + APPLICATION + "</Application>" +
					"<Workflow>" + WORKFLOW + "</Workflow>" +
					"<PageFile>transaction.xml</PageFile>" +
					"<Rulesets>" + RULESETS + "</Rulesets>" +
					"</Properties>";
			String urlExecuteRules = urlOCR +  "Transaction/Execute";
			HttpPost httpPostExecuteRules = new HttpPost(urlExecuteRules);
			httpPostExecuteRules.setEntity(new StringEntity(executeRules));
			httpPostExecuteRules.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseExecuteRules = httpclient.execute(httpPostExecuteRules);
			StatusLine statusLineExecuteRules = responseExecuteRules.getStatusLine();
			responseUploadPageFile.close();
			logger.info("execute rules status: " + statusLineExecuteRules.getStatusCode());
			
			//get a file produced by rules
			HttpGet httpGetFile = new HttpGet(urlOCR + "Transaction/GetFile/" + transactionId + "/" + IMAGENAME + "/txt");
			CloseableHttpResponse responseGetFile = httpclient.execute(httpGetFile);
			HttpEntity entityGetFile = responseGetFile.getEntity();
			String getFileText = "";
			
			if (entityGetFile != null) {
				// get entity contents and convert it to string
				InputStream	instream = entityGetFile.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(instream,"utf8"),8192);
				String str;
				while ((str = br.readLine())!=null)
				{
					getFileText +=str;
				}
				//getFileText = EntityUtils.toString(entityGetFile);
				instream.close();
			}	
			StatusLine statusLineGetFile = responseExecuteRules.getStatusLine();
			responseGetFile.close();
			logger.info("get file status: " + statusLineGetFile.getStatusCode());
			logger.info("get file text:");
			logger.info("*****************************");
			logger.info(getFileText);
			logger.info("*****************************");
			result = getFileText;
			
			//end transaction
			HttpDelete httpEnd = new HttpDelete(urlOCR + "Transaction/End/"+transactionId);
			CloseableHttpResponse responseEnd = httpclient.execute(httpEnd);
			StatusLine statusLineEnd = responseEnd.getStatusLine();
			responseEnd.close();
			logger.info("end status: " + statusLineEnd.getStatusCode());
			
			//logoff
			String urlLogoff = urlOCR +  "Session/Logoff";
			HttpPost httpPostLogoff = new HttpPost(urlLogoff);
			httpPostLogoff.setEntity(new StringEntity(""));
			httpPostLogoff.setHeader("Content-Type", "text/xml");
			CloseableHttpResponse responseLogoff = httpclient.execute(httpPostLogoff);
			StatusLine statusLineLogoff = responseLogon.getStatusLine();
			responseLogoff.close();
			logger.info("logoff status: " + statusLineLogoff.getStatusCode());	

		} catch (Exception e) {
			logger.info("erreur d'invocation de la brique OCR" + e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
}

