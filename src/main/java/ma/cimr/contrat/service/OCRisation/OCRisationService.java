package ma.cimr.contrat.service.OCRisation;

import org.springframework.web.multipart.MultipartFile;

public interface OCRisationService {

	public String getFileText(MultipartFile file);
	
	
}
